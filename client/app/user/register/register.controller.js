angular.module('artoo').controller('RegisterCtrl',function($scope,UserService,$state){
    $scope.UserService=UserService;
    $scope.user;
    console.log($scope.user);
      $scope.register = function (user) {
        $scope.user = user;
        console.log($scope.user);
      
        $state.go("user.register.measures");
    };   
    
    
    $scope.measures = function (usermeasure)  {
        console.log('click');
        
        $scope.user.measures=usermeasure;
        UserService.register($scope.user);
        console.log($scope.user);
        
        $state.go("user.profile",{id:$scope.user.id});
        $scope.user={};
    }
    
})