angular.module('artoo')
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');
  
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.template.html'
      })
      
      .state('user', {
        url: '/user',
        templateUrl: 'app/user/user.template.html',
        controller: 'UserCtrl',
      })

      .state('user.login', {
        url: '/login',
        templateUrl: 'app/user/login/login.template.html',
        controller: 'LoginCtrl',
      })

      .state('user.register', {
        abstract: true,
        url: '/register',
        templateUrl: 'app/user/register/register.template.html',
        controller: 'RegisterCtrl'
      })

      .state('user.register.measures', {
        url: '/measures',
        templateUrl: 'app/user/register/measures/measures.template.html',
      })
    
      .state('user.register.registration', {
        url: '/registration',
        templateUrl: 'app/user/register/registration/registration.template.html',
      })

      .state('user.profile', {
        url: '/profile/:id',
        templateUrl: 'app/user/profile/profile.template.html',
        controller: 'ProfileCtrl',
        resolve: {
          users: function($stateParams, UserService) {
            return UserService.GetUserById(parseInt($stateParams.id));
          }
        }
      })
    
      .state('partners', {
        abstract: true,
        url: '/partners',
        templateUrl: 'app/partners/partners.template.html'
      })

      .state('partners.info', {
        url: '/info/:id',
        templateUrl: 'app/partners/info/info.partners.template.html',
        controller: 'PartnersInfoCtrl',
        resolve: {
          partner: function ($stateParams, $log, Partners) {
            return Partners.getPartnerById($stateParams.id);
          }
        }
      })

      .state('partners.measures', {
        url: '/measures',
        templateUrl: 'app/partners/measures/measures.partners.template.html',
        controller: 'PartnersMeasuresCtrl'
      })

      .state('partners.list', {
        url: '/list',
        templateUrl: 'app/partners/list/list.partners.template.html',
        controller: 'PartnersListCtrl'
      });
  });