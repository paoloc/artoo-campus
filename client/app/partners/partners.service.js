angular.module('artoo').factory('Partners', function() {
    //modello partners affiliati
    var partner1 = {
        id: 1,
        name: 'Zara',
        vat_code: 'pi1234',
        cod_fisc: 'cf123',
        address: 'Via Roma',
        zip_code: '60121',
        city: 'Ancona',
        prov: 'AN',
        country: 'Italy',
        email: 'zara@esempio.it',
        username: 'zara',
        pass: 'pass1234',
        active: 1,
        ultimo_acc: '28/10/2015',
        data_reg: '27/10/2015'
    };
    
    var partner2 = {
        id: 2,
        name: 'Nike',
        vat_code: 'pi567',
        cod_fisc: 'cf456',
        address: 'Via Garibaldi',
        zip_code: '40121',
        city: 'Bologna',
        prov: 'BO',
        country: 'Italy',
        email: 'nike@esempio.it',
        username: 'nike',
        pass: 'pass456',
        active: 1,
        ultimo_acc: '28/10/2015',
        data_reg: '27/10/2015'
    };
    
    var partners = [partner1, partner2];
    
    var getAllPartners = function () {
        return partners;
    };
    
    var getPartnerById = function (id) {
        var result = partners.filter(function(partner) {
            return partner.id === parseInt(id);
        });
        return (result.length && result[0]) || -1;
    };
    
    var updatePartner = function (partner) {
        var result = getPartnerById(partner.id);
        var i = partners.indexOf(result);
        partners[i] = partner;
    };
    
    var addPartner = function (partner) {
        var idCorrente = partners.length + 1;
        partner.id = idCorrente;
        partners.push(partner);
        console.log(partners);
    };
    
    var removePartner = function (partner) {
        partners = partners.filter(function (item) {
            return partner.id !== item.id;
        });
        
    };
    
    //public
    return {
        addPartner: addPartner,
        getPartnerById: getPartnerById,
        getAllPartners: getAllPartners,
        removePartner: removePartner,
        updatePartner: updatePartner
    };
});