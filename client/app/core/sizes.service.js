angular.module('artoo').factory('SizesSrv', function() {

  /**
   * Object contains sizes' percentage
   * */
  var percentageSizes = {
    xs: 0,
    s: 0,
    m: 0,
    l: 0,
    xl: 0,
    xxl: 0
  };
    
  /** 
   * Percentage top body
   * clothes: shirt, sweater, jeans, trousers, dress, coat, etc
   * */
  var allTypologies = { 
    tshirt: {
      neck : 5,
      shoulders: 20,
      busto: 20,
      waist: 25,
      lunghezzabraccio: 5,
      polso: 5,
      hips: 10,
      altezzabusto: 5
    }
  };

  /** 
   * Percentage top bottom
   * */    
  var percBottom = {
    esternogamba: 20,
    fianchi: 80
  };
    
  /**
   * Get the object of typology
   * @param String
   * return Object typology
   * */
  var currentTypology= "tshirt";
  var typology = function(currentTypology) {
    if (allTypologies.hasOwnProperty(currentTypology)) {
      return allTypologies[currentTypology];
    }
  };
    
  /**
   * Match between properties and values
   * @param Object user 
   * @param Object partner
   * return Object final percentage sizes
   * */
  var filterProp = function(user, partner) {
    var finalSizes;
    for (var prop in user) {
      var value = (user[prop]);
      
      if (partner.hasOwnProperty(prop)) {
        var clothesTypology = typology(currentTypology);
        finalSizes = getSize(partner[prop], value, prop, clothesTypology);
      }
    }
    
    console.log(finalSizes);
    return finalSizes;
  };
   
  /**
   * Check values prop of user with partner
   * @params Object RangeError
   * @params user params
   * @params name user params
   * @params clothes
   * return Object percentage sizes 
   * */
  var getSize = function(prop, value, name, clothesTypology){
    for (var i in prop) {
      if (value >= prop[i].min && value <= prop[i].max) {
        percentageSizes[i] += clothesTypology[name];
        // console.log('Il massimo di ' + prop[i].name + ' e\' ' + prop[i].max);
      }
    }
    
    return percentageSizes;
  };
    
  // public API
  return {
    filterProp: filterProp
  };
});
    